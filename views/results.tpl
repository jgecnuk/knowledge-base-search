%include('header', title=": " + query['query']+" ("+str(nres)+")")
%include('search', query=query, dirs=dirs, sorts=sorts, config=config)

%include('pages', query=query, config=config, nres=nres)
<script>

    let occurenceCounts = []
    let numberOfDirs = {{len(dirs)}}  - 2
    let loadedOccurences = 0

    const loadOccurenceCountForDir = async (dir, query) => {
        let occurenceCount = await fetch("/resultsPerDir?" + new URLSearchParams({query, dir})); //Inside the function, it makes a `fetch` request to the server at the URL `/resultsPerDir` with the query parameters `query` and `dir`. The response from the server is stored in the `occurenceCount` variable.
        let count = await occurenceCount.json(); //The `json()` method is then called on `occurenceCount` to parse the response as JSON and store the result in the `count` variable.
        let counterSpan = document.getElementById("counter-"+dir); //Function retrieves an element from the DOM with the ID `counter-dir` (where `dir` is the value of the `dir` parameter) and sets its `innerText` property to the value of `count.nres`. This updates the counter display for the specified directory with the number of search results found in that directory.
        counterSpan.innerText = count.nres //This updates the counter display for the specified directory with the number of search results found in that directory.

        if (!occurenceCounts.find(value => value.dir === dir)) {
            occurenceCounts.push({dir, count: count.nres})
        }

        loadedOccurences++;
        if (loadedOccurences === numberOfDirs) {
            console.log("Loaded all " + numberOfDirs + " diectories occurence count")

            // sort buttons
            let buttons = document.getElementById('chips').children;

            let toSort = Array.prototype.slice.call(buttons, 0);
            toSort.sort((a, b) => {
                let idA = a.id.split("button-dir-")[1];
                let idB = b.id.split("button-dir-")[1];
                let countsA = occurenceCounts.find(count => count.dir === idA);
                let countsB = occurenceCounts.find(count => count.dir === idB);

                return countsB.count - countsA.count
            })

            let buttonsParent = document.getElementById('chips');
            buttonsParent.innerHTML = ''

            toSort.forEach(button => {
                buttonsParent.appendChild(button)
            })

            // sort folders
            let folders = document.getElementById('horizontal').children;
            let toSortFolders = Array.prototype.slice.call(folders, 0);
            toSortFolders.sort((a, b) => {
                let idA = a.id.split("results-dir-")[1];
                let idB = b.id.split("results-dir-")[1];
                let countsA = occurenceCounts.find(count => count.dir === idA);
                let countsB = occurenceCounts.find(count => count.dir === idB);

                return countsB.count - countsA.count
            })

            let foldersParent = document.getElementById('horizontal');
            foldersParent.innerHTML = ''

             toSortFolders.forEach(folder => {
                 foldersParent.appendChild(folder)
            })
        }
    }

    const copyToClipboard = (content) => {
        navigator.clipboard.writeText(content);
        //alert("Copied path to clipboard: " + content);
    }



</script>

<script>
    window.addEventListener('scroll', function() {
        var label = document.getElementById('floating-label');
        if (!label.classList.contains('transparent')) {
            label.classList.add('transparent');
        }
    });

    window.addEventListener('keydown', function(event) {
        if (event.shiftKey) {
            var label = document.getElementById('floating-label');
            if (!label.classList.contains('transparent')) {
                label.classList.add('transparent');
            }
        }
    });
</script>

%dirs =  sorted(dirs, key=str.lower)
%for d in range(2, len(dirs)):
<script>
    loadOccurenceCountForDir('{{dirs[d]}}', "{{query['query']}}")
</script>
%end

<div id="horizontal">
    %for d in range(2, len(dirs)):
        <div id="results-dir-{{dirs[d]}}">
        %nfilter = dirs.index(dirs[d])
            <div id="found" >

                %alias= str(dirs[d]).lstrip(dirs[1])
                %alias=alias.lstrip('/')
                <b id="alias-{{alias}}" style="border-bottom: solid 5px;">{{alias}}</b>
                <b class="counter" id="counter-{{dirs[d]}}" style="margin-left: 2rem">{{nres}}</b>
                <b style="margin-left: 2rem">  </b>

                %maxchars = 50
                %nchars = maxchars - len(alias)
                %if len(alias) + len(qs) > maxchars:
                    %shorterqs = qs[:nchars]
                    %shorterqs = shorterqs + "..."
                %else:
                    %shorterqs=qs
                %end

                Search:   <b><i>{{shorterqs}}</i></b>
                <!-- <b style="text-align: right; margin-left: 8rem">sort:  <span class="sortdesc"><img src = "static/icondown.svg" alt="Descending sort"  width="20px" /> </span></b> -->
            </div>
            %query["dir"] = dirs[d]
            %is_empty= 1
            %for i in range(0, len(res)):
                %unwanted_keys = ["<all>", "KBS"]
                %if dirs[d] in res[i]["url"] and dirs[d] not in unwanted_keys:
                    %is_empty= 0
                    %include('result', d=res[i], i=i, query=query, config=config, query_string=query_string, dirname=dirs[d])
                %end
            %end
            <script>
                if ({{is_empty}}) {
                    document.getElementById("results-dir-{{dirs[d]}}").style.display = "none"
                    document.getElementById("button-dir-{{dirs[d]}}").setAttribute("disabled", '')
                }
            </script>
        </div>
    %end
</div>

<div id="floating-label">
    <b> Hold Shift to scroll horizontally <--> </b>
</div>
%include('pages', query=query, config=config, nres=nres)
%include('footer')



<!-- Modal -->
<dialog id="preview-modal">
    <article class="big-modal">
        <a href="#close"
           aria-label="Close"
           class="close"
           data-target="preview-modal"
           onClick="toggleModal(event)">
        </a>
        <div id="modal-body">

        </div>
    </article>
</dialog>

<dialog id="open-view-modal">
    <article class="big-modal">
        <a href="#close"
           aria-label="Close"
           class="close"
           data-target="open-view-modal"
           onClick="toggleModal(event)">
        </a>
        <div id="modal-open-body">
            <iframe src="/static/ViewerJS/index.html#"
                    allowfullscreen
                    webkitallowfullscreen
                    id="modal-iframe"
                    style="width: 100%;height: 100%"

            ></iframe>
        </div>
    </article>
</dialog>
<!-- vim: fdm=marker:tw=80:ts=4:sw=4:sts=4:et:ai
-->
<script>
    var modal = document.getElementById("modal-window");
    var modalBody = document.getElementById("modal-body");
    var modalOpen = document.getElementById("modal-window-open");
    var modalBodyOpenIframe = document.getElementById("modal-iframe");


    var openModalPreview = async (content, event) => {
        toggleModal(event)
        //modal.style.display = "block"
        let response = await fetch(content);
        let s = await response.text();
        modalBody.innerHTML = s
    }

    var closeModalPreview = () => {
        modal.style.display = "none"
    }

    var openModalOpen = (url, event) => {
        toggleModal(event)
        // modalOpen.style.display = "block"
        modalBodyOpenIframe.src = '/static/ViewerJS/index.html#/' + url
        modalBodyOpenIframe.contentWindow.location.reload()
    }
    /*
    var openModalOpenPage = (url, pagestart) => {
        modalOpen.style.display = "block"
        modalBodyOpenIframe.src = '/static/ViewerJS/index.html?startpage=' + pagestart + "#/" url
        modalBodyOpenIframe.contentWindow.location.reload()
        console.log(modalBodyOpenIframe.src)
    }
     */
    var closeModalOpen = () => {
        modalOpen.style.display = "none"
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }

        if (event.target == modalOpen) {
            modalOpen.style.display = "none"
        }
    }
</script>
