<article style="width: 1000px">
    <header style="border-bottom: var(--border-width) solid var(--card-border-color);"> Information about Knowledgescope  </header>
    <h2>Full-text <mark class="search-result-highlight">search</mark> across all files stored on the shared drive, git repositories and wiki.</h2>
    <h4>See all results in  <mark class="search-result-highlight">parallel</mark>, filter them and find what you are looking for. </h4>

    <p>Usually it would be used for search in your files, like documents, wiki, code and images. <br>
    For demonstration purpouses, you can search through few <mark class="search-result-highlight"> diploma works</mark> which originated at different universities, archive of animation studio and currenty displayed work:
    </p>
    <div role="button" class="contrast" style="background: #B2EBF2;">Currently displayed works of Departement of Graphics at UMPRUM 2023 (GRAFIKA 2023)</div>
    <div role="button" class="contrast" style="background: #D1C4E9;">Departement of Art History at Charles University (FFUK)</div>
    <div role="button" class="contrast" style="background: #E1BEE7;">FAVU (AGD1, AGD2)</div>
    <div role="button" class="contrast" style="background: #BBDEFB;">Archive of Animation and Film Studio (A70)</div>
    <div role="button" class="contrast" style="background: #FFCDD2;">UMPRUM 2022-2020</div>






</article>

<article style="width: 1000px">
    <header style="border-bottom: var(--border-width) solid var(--card-border-color);"> How to start? </header>
    <h2>Try search something: </h2>
    <p>It can be general like <mark class="search-result-highlight">"user interface"</mark>, <mark class="search-result-highlight">"animation"</mark>, <mark class="search-result-highlight">"recyklace"</mark>, <mark class="search-result-highlight">"sociální sítě"</mark>, or you can be specific and search for author, like <mark class="search-result-highlight">"Ted Nelson"</mark>. Maybe you will find something that diploma works have in common.</p>


</article>