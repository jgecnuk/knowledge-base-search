<html>
<head>
    <title>Knowledgescope {{title}}</title>
    <!--
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
    -->
    <link rel="stylesheet" type="text/css" href="static/css/pico.css">
    <!--
    <link rel="stylesheet" type="text/css" href="static/style.css">
    -->
    <link rel="stylesheet" type="text/css" href="static/ourStyle.css">
    <script type="text/javascript" src="static/modal.js"></script>
    <script type="text/javascript" src="static/jquery.js"></script>
    <script type="text/javascript" src="static/extra.js"></script>

    <script type="text/javascript" src="static/jdpicker.js"></script>
    <link rel="stylesheet" type="text/css" href="static/jdpicker.css">
    <link rel="icon" type="image/png" href="static/icon.png">
    <link rel="search" type="application/opensearchdescription+xml" title="Knowledge Kaleidoscope" href="osd.xml">
</head>
<body>
    <main class="container-fluid">
