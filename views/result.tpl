%import shlex, unicodedata, os, re

<article class="search-result">
    <header id="folder-header-{{dirname}}">
        <script>
            document.getElementById("alias-{{alias}}").style.borderColor = getColor('{{dirname}}');
        </script>
    %number = (query['page'] - 1)*config['perpage'] + i + 1

    %url = d['url'].replace('file://', '')
    %for dr, prefix in config['mounts'].items():
        %url = url.replace(dr, prefix)
    %end

    <div class="search-result-url">
        %urllabel = os.path.dirname(d['url'].replace('file://', ''))
        %for r in config['dirs']:
            %urllabelshorter = urllabel.replace(r.rsplit('/',1)[0] + '/' , '')
            %parentfolder = urllabel.replace(r,'')
            %parentfolder = urllabel.replace(" ","")

            %displayedpath = parentfolder + '/' + d['filename']
            %displayedpath = displayedpath.replace(" ","")
            %nmax = 37
            %filename = d['filename']
            %filename = filename.replace(" ","")
            %nchars = nmax - len(filename)
            %if len(displayedpath) > nchars:
                %parentfolder = parentfolder [-nchars:]
                %parentfolder = "..." +parentfolder
            %end
            %if ( len(parentfolder) + len(filename) ) > nmax +3:
                %filename = filename [:nmax]
                %filename = filename + "..."
                %parentfolder= ""
            %end
        %end
    </div>

    <div class="search-result-title myheader" id="r{{d['sha']}}" title="{{displayedpath}}">
        %if 'title_link' in config and config['title_link'] != 'download':
            %if config['title_link'] == 'open':
                <a href="{{url}}">{{d['filename']}}</a>
            %elif config['title_link'] == 'preview':
                <a href="preview/{{number-1}}?{{query_string}}">{{parentfolder}}/<b>{{filename}}</b></a>
            %end
        %else:
        <div class="element-1" style="overflow-x: visible;"><a href="download/{{number-1}}?{{query_string}}">{{parentfolder}}/<b>{{filename}}</b></a></div>
        <div class="element-2">
            <button data-tooltip="Copy Path to the clipboard"
                    data-placement="bottom"
                    type="button"
                    role="button"
                    id="copy"
                    style= "width:2rem;height:2rem;border-color:transparent;color:var(--my-very-dark-grey);border-radius: 0; background-color:var(--my-card-sectionning-background-color-darker);margin-bottom: 0px; align-items: center; justify-content: center; margin:auto;"
                    onclick="copyToClipboard('{{urllabel}}/{{d['filename']}}')">
                <svg class="svg-icon"
                     height="20"
                     width="20"
                     viewBox="0 0 16 16"
                     aria-labelledby="button-label"
                     focusable="false">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8 2.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM9.45 2a2.5 2.5 0 0 0-4.9 0H3a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h2v-1.5H3.5v-9h1V5h5V3.5h1V7H12V3a1 1 0 0 0-1-1H9.45zM7.5 9.5h1.25a.75.75 0 0 0 0-1.5h-1.5C6.56 8 6 8.56 6 9.25v1.5a.75.75 0 0 0 1.5 0V9.5zm1.25 5H7.5v-1.25a.75.75 0 0 0-1.5 0v1.5c0 .69.56 1.25 1.25 1.25h1.5a.75.75 0 0 0 0-1.5zm3.75-5h-1.25a.75.75 0 0 1 0-1.5h1.5c.69 0 1.25.56 1.25 1.25v1.5a.75.75 0 0 1-1.5 0V9.5zm-1.25 5h1.25v-1.25a.75.75 0 0 1 1.5 0v1.5c0 .69-.56 1.25-1.25 1.25h-1.5a.75.75 0 0 1 0-1.5z"></path>
                </svg>
            </button>
        </div>
        <div class="element-3">{{d['time'].replace(' ','. ')}}</div>
        %end
    </div>

    <div class="search-result-links">
        <a onClick="openModalOpen('download/{{number-1}}?{{query_string}}', event)"
           data-target="open-view-modal">Open</a>
        <a href="download/{{number-1}}?{{query_string}}">Download</a>
        <a onClick="openModalPreview('preview/{{number-1}}?{{query_string}}', event)"
                data-target="preview-modal">Preview</a>
    </div>
    </header>

    <div class="search-result-snippet">
        %if (".pdf" in d['filename']):
            %pagestart = re.findall('[0-9]+', d['snippet'])
            %if pagestart:
                <details>
                    <summary role="button" class="secondary">View pdf here</summary>
                    %link =  '/static/ViewerJS/index.html#/' + 'download/'+ str(number-1) + '?' +query_string
                    %link = '/static/ViewerJS/index.html?startpage=' + pagestart[0] + '#/download/'+ str(number-1) + '?' +query_string
                    <iframe style="float:right;margin-bottom:1rem;" src={{link}} width='650' height='600' allowfullscreen webkitallowfullscreen navbar="false"></iframe>
                </details>
            %end
        %end


        %snip=d['snippet'].replace("[p","<b>[s. ")
        %snip=snip.replace("] ","] </b>")
        %snip=snip.replace("...","̈́... <br><hr><br>")
        %snip=snip.replace("�","")

        %if (".JPG" in d['filename'] or ".jpg" in d['filename'] or ".png" in d['filename'] or ".jpeg" in d['filename']):
            %snip =""
            %imgquery = "download/" + str(number-1) + "?" + query_string
            <img src="{{imgquery}}" width="750px"">
        %end
        {{!snip}}
    </div>

</article>
<!-- vim: fdm=marker:tw=80:ts=4:sw=4:sts=4:et:ai
-->
