<?xml version="1.0" encoding="UTF-8"?>
<OpenSearchDescription  xmlns="http://a9.com/-/spec/opensearch/1.1/"
                        xmlns:moz="http://www.mozilla.org/2006/browser/search/">
  <ShortName>Knowledgescope</ShortName>
  <Description>Interface for knowledge base search with paraller view</Description>
  <Url type="text/html" template="{{url}}/results?query={searchTerms}"/>
  <Image width="48" height="48">{{url}}/static/icon.png</Image>
  <InputEncoding>UTF-8</InputEncoding>
  <moz:SearchForm>{{url}}/</moz:SearchForm>
</OpenSearchDescription>
