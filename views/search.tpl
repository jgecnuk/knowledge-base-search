%import re

<script>
    const generateColorPalette = () => {
      const colorsOrig = ['#feddbb','#bbfdfe','#ddbbfe','#ffcdd2','#f8bbd0','#e1bee7','#d1c4e9','#c5cae9','#bbdefb','#b3e5fc','#b2ebf2','#b2dfdb','#c8e6c9','#dcedc8','#f0f4c3','#fff9c4','#ffecb3','#ffe0b2','#ffccbc'];
      const colors200 =['#EF9A9A','#F48FB1','#CE93D8','#B39DDB','#9FA8DA','#90CAF9','#81D4FA','#80DEEA','#80CBC4','#A5D6A7','#C5E1A5','#E6EE9C','#FFF59D','#FFE082','#FFCC80'];
      const colors100 =['#FFCDD2','#F8BBD0','#E1BEE7','#D1C4E9','#C5CAE9','#BBDEFB','#B2EBF2','#B2DFDB','#C8E6C9','#DCEDC8','#F0F4C3','#FFF9C4','#FFECB3','#FFE0B2','#FFCCBC'];
      const colors50 =['#FFEBEE','#FCE4EC','#F3E5F5','#EDE7F6','#E8EAF6','#E3F2FD','#E0F7FA','#E0F2F1','#E8F5E9','#F1F8E9','#F9FBE7','#FFFDE7','#FFF8E1','#FFF3E0','#FBE9E7'];
      const colors = colors100;
      const colorPalette = {};

      let index = 0;
      return (dirname) => {
        if (!colorPalette.hasOwnProperty(dirname)) {
          colorPalette[dirname] = colors[index % colors.length];
          index++;
        }
        return colorPalette[dirname];
      };
    };
    // Create a color generator
    const getColor = generateColorPalette();

    const LOCAL_STORAGE_UNSELECTED_DIRS = "unselectedDirs" //string used as a key to store and retrieve data from the browser's local storage.

    const setCheckmarks = (dirname, checked) => { //checked` which is a boolean indicating whether the directory is checked or not.
        let checkmark = document.getElementById("button-dir-checkmark-" + dirname);
        let button = document.getElementById("button-dir-" + dirname);// Inside the function, it retrieves three elements from the DOM using `document.getElementById`: `checkmark`, `button`, and `column`. It then updates the style and class of these elements based on the value of `checked`.
        let column = document.getElementById("results-dir-" + dirname);

        const color = getColor(dirname); // Get the color for the dirname

        if (checked) { //If `checked` is true, the checkmark element is displayed, the button element's class is set to "contrast", and the column element is displayed
            checkmark.style.display = "inline"
            button.className = "contrast"
            button.style.backgroundColor = color;
            if (column) {
                column.style.display = "block"
            }
        } else {
            checkmark.style.display = "none"
            button.className = "contrast outline"
            button.style.backgroundColor = "";
            if (column) {
                column.style.display = "none"
            }
        }
    }

    const resetCheckmarks = () => {
        localStorage.setItem(LOCAL_STORAGE_UNSELECTED_DIRS, null)
    }

    // check state on init
    $(document).ready(function() { //This code uses jQuery to run a function when the document is ready.
        let unselectedDirsInitial = JSON.parse(localStorage.getItem(LOCAL_STORAGE_UNSELECTED_DIRS)); // it retrieves the value of a variable `unselectedDirsInitial` from the browser's local storage using `localStorage.getItem()`. The value is parsed as JSON using the `JSON.parse()` method.
        console.log({unselectedDirsInitial}) // log the value of `unselectedDirsInitial` to the console for debugging purposes.
        if (unselectedDirsInitial) { //If `unselectedDirsInitial` is not `null`, the function calls `setCheckmarks` for each directory name in `unselectedDirsInitial` with the second parameter set to `false`. This is used to ensure that the checkmarks for any previously unselected directories are removed when the page is reloaded.
            unselectedDirsInitial.forEach(dirname => {
                setCheckmarks(dirname, false)
            })
        }
    });

    const selectDir = (dirname) => { //this function is used to toggle the selection of a directory and update the `unselectedDirs` array in the browser's local storage accordingly.
        let unselectedDirs = JSON.parse(localStorage.getItem(LOCAL_STORAGE_UNSELECTED_DIRS));
        if (!unselectedDirs) {
            unselectedDirs = []
        }
        let alreadyUnselectedDir = unselectedDirs.indexOf(dirname);
        if (alreadyUnselectedDir !== -1) {
            // is already unselected - select it
            unselectedDirs.splice(alreadyUnselectedDir, 1)
            setCheckmarks(dirname, true)
        } else {
            // basic state - we need to unselect it
            unselectedDirs.push(dirname)
            setCheckmarks(dirname, false)
        }
        localStorage.setItem(LOCAL_STORAGE_UNSELECTED_DIRS, JSON.stringify(unselectedDirs))
    }

</script>
<div id="searchbox">
<form action="results" method="get" style="margin-bottom:0px;"  autocomplete="off">
<h2>Search internal knowledge base with Knowledgescope</h2>
<div class="grid">
    <input tabindex="0" type="search" name="query" value="{{query['query']}}" placeholder="Typehere, you can use logical operators like OR and AND" autofocus width="70%">
    <div class="grid">
        <input type="submit" value="Search" class="contrast outline">&nbsp;
        <a href="./" tabindex="-1"><input type="button" value="Reset" class="contrast outline" onclick="resetCheckmarks()">
        </a>&nbsp;
        %if not config['rclc_nosettings']:
            <!-- <a href="settings" tabindex="-1"><input type="button" value="Settings" class="contrast outline"></a> -->
        %end
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>

<div id="chips" >
%for d in range(2, len(dirs)):
    %nfilter = dirs.index(dirs[d])
    <div role="button" class="contrast chips" onclick="selectDir('{{dirs[d]}}')" id="button-dir-{{dirs[d]}}">
        <script>
            document.getElementById("button-dir-{{dirs[d]}}").style.backgroundColor = getColor('{{dirs[d]}}');
        </script>
        <span class="closebtn" id="button-dir-checkmark-{{dirs[d]}}"><img src = "static/iconcheck.svg" alt="checked" width="20px"/>
        </span>
        %alias= str(dirs[d]).lstrip(dirs[1])
        %alias=alias.lstrip('/')
        {{alias}}
    </div>
%end
</div>

<input type="hidden" name="page" value="1" />
</form>
</div>
<hr>
<!-- vim: fdm=marker:tw=80:ts=4:sw=4:sts=4:et:ai
-->
